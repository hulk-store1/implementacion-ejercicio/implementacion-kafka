# implementacion-kafka
Creacion maquina virtual EC2 Ubuntu v22.04
sobre esta maquina se realizará el siguiente procedimiento:
1. instalacion del stack de elastic (elastic search, kibana, logstash, filebeat en ese orden).
2. instalacion de Kafka
3. creacion de topic llamado "topic-implementacion" en kafka
4. crear archivo en formato .json y usar tail -f para verificar sus cambios en los logs.
5. configurar input en filebeat tipo file para el archivo en formato .json
6. configuracion del archivo output de filebeat para que apunte al topic recien creado funcionando como producer.
7. configuracion del archivo input de logstash para que funcione como subscrictor del topic creado.
8. procesado en logstash para cambio de formato.
